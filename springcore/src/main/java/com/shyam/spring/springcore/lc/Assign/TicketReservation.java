package com.shyam.spring.springcore.lc.Assign;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

public class TicketReservation {
	private int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	@Override
	public String toString() {
		return "TicketReservation [id=" + id + "]";
	}

	@PostConstruct
	public void intialize() {
		System.out.println("Inside the initialize method");
	}
	
	@PreDestroy
	public void cleanUp() {
		System.out.println("Inside the cleanUp method");
	}

}
