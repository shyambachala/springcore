package com.shyam.spring.springcore.list;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {

	public static void main(String[] args) {

		ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext(
				"com/shyam/spring/springcore/list/listconfig.xml");
		Hospital hospital = (Hospital) ctx.getBean("hospital");
		System.out.println("Name: " + hospital.getName());
		System.out.println("Departments: " + hospital.getDepartments());
		System.out.println("Departments: " + hospital.getDepartments().getClass());

	}

}
