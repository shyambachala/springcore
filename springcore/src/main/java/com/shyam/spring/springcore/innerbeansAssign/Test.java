package com.shyam.spring.springcore.innerbeansAssign;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ApplicationContext ctx = new ClassPathXmlApplicationContext(
				"com/shyam/spring/springcore/innerbeansAssign/config.xml");
		University bean = (University) ctx.getBean("university");
		System.out.println(bean.hashCode());

		University bean1 = (University) ctx.getBean("university");
		System.out.println(bean1.hashCode());
		System.out.println(bean1);
	}

}
