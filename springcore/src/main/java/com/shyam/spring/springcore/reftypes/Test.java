package com.shyam.spring.springcore.reftypes;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {

	public static void main(String[] args) {
		ApplicationContext ctx = new ClassPathXmlApplicationContext("com/shyam/spring/springcore/reftypes/reftypesconfig.xml");
		Student bean = (Student) ctx.getBean("student");
//		System.out.println("M:"+bean.getScores().getMaths());
//		System.out.println("P:"+bean.getScores().getPhysics());
//		System.out.println("C:"+bean.getScores().getChemistry());
		System.out.println(bean);
	}

}
