package com.shyam.spring.springcore.set;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {

	public static void main(String[] args) {

		ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext(
				"com/shyam/spring/springcore/set/setconfig.xml");
		CarDealer bean = (CarDealer) ctx.getBean("car");
		System.out.println("Name: " + bean.getName());
		System.out.println("Models: " + bean.getModels());

	}

}
