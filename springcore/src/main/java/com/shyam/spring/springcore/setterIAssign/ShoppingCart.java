package com.shyam.spring.springcore.setterIAssign;

import java.util.List;

public class ShoppingCart {
	private List<Item> item;

	public List<Item> getItem() {
		return item;
	}

	@Override
	public String toString() {
		return "ShoppingCart [item=" + item + "]";
	}

	public void setItem(List<Item> item) {
		this.item = item;
	}
}
