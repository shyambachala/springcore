package com.shyam.spring.springcore.setterIAssign;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {

	public static void main(String[] args) {

		ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext(
				"com/shyam/spring/springcore/setterIAssign/setterIAssignconfig.xml");
		ShoppingCart bean = (ShoppingCart) ctx.getBean("shop");
		System.out.println(bean);

	}

}
