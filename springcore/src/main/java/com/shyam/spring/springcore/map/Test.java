package com.shyam.spring.springcore.map;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {

	public static void main(String[] args) {
		ApplicationContext ctx = new ClassPathXmlApplicationContext("com/shyam/spring/springcore/map/mapconfig.xml");
		Customer bean = (Customer) ctx.getBean("customer");
		System.out.println("Id: "+bean.getId());
		System.out.println("Name: "+bean.toString());
	}

}
