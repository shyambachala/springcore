package com.shyam.spring.springcore.lc.Assign;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		AbstractApplicationContext context = new ClassPathXmlApplicationContext(
				"com/shyam/spring/springcore/lc/Assign/config.xml");
		TicketReservation bean = (TicketReservation) context.getBean("ticket");
		System.out.println(bean);
		context.registerShutdownHook();
	}

}
