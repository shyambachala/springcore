package com.shyam.spring.springcore.dependencycheck;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ApplicationContext ctx = new ClassPathXmlApplicationContext(
				"com/shyam/spring/springcore/dependencycheck/config.xml");
		Prescription bean = (Prescription) ctx.getBean("prescription");
		System.out.println(bean);

	
	}

}
