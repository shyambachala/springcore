package com.shyam.spring.springcore.properties;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {

	public static void main(String[] args) {
		ApplicationContext ctx = new ClassPathXmlApplicationContext("com/shyam/spring/springcore/properties/propertiesconfig.xml");
		CountriesAndLanguages bean = (CountriesAndLanguages) ctx.getBean("props");
		System.out.println(bean.getCountryAndLangs());
		
	}

}
