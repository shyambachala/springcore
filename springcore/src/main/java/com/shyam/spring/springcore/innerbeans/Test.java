package com.shyam.spring.springcore.innerbeans;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ApplicationContext ctx = new ClassPathXmlApplicationContext(
				"com/shyam/spring/springcore/innerbeans/config.xml");
		Employee bean = (Employee) ctx.getBean("employee");
		System.out.println(bean.hashCode());

		Employee bean1 = (Employee) ctx.getBean("employee");
		System.out.println(bean1.hashCode());
	}

}
